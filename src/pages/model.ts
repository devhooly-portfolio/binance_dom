import { defineStore } from 'pinia'
import { ref } from 'vue'
import { DEFAULT_CURRENCY_PAIR, DEFAULT_DEPTH } from '@shared/const'

export const useSettingsStore = defineStore('settings', () => {
  const selectedPair = ref<string>(DEFAULT_CURRENCY_PAIR)
  const selectedDepth = ref<number>(DEFAULT_DEPTH)

  return {
    selectedPair,
    selectedDepth
  }
})
