import { createPinia } from 'pinia'
import { createApp } from 'vue'
import 'vuetify/styles'
import { createVuetify } from 'vuetify'
import { BREAKPOINT_KEY } from '@shared/const'
import App from './App.vue'
import { breakpointsProvider } from './providers'
import { router } from './router'

const vuetify = createVuetify({
  theme: {
    defaultTheme: 'dark'
  }
})

const app = createApp(App)
app
  .use(createPinia())
  .use(router)
  .use(vuetify)
  .provide(BREAKPOINT_KEY, breakpointsProvider())
  .mount('#app')
