## [Live demo here](https://devhooly-portfolio.gitlab.io/binance_dom/)
## Test task: binance stock book


Full techical requirements here - [ru](description/ru.md) / [en](description/en.md). The entire narrative has been preserved.

## Stack
- Vue3
- Vite
- css modules
- vue-router

## Architecture inspired by [Feature sliced design](https://feature-sliced.design/)

![slices img](https://feature-sliced.design/assets/ideal-img/visual_schema.b6c18f6.1030.jpg)