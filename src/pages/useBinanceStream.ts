import { useWebSocket } from '@vueuse/core'
import { shallowRef } from 'vue'
import { getSnapshot, type PriceRow } from '@shared/api'
import { type ModifiedRow, modifyRows } from './modifyRows'

type Event = {
  e: 'depthUpdate'
  E: number
  s: string
  U: number
  u: number
  b: PriceRow[]
  a: PriceRow[]
}

export const useBinanceStream = async () => {
  const snapshot = await getSnapshot('BTCUSDT', 100)

  let isFirstEventReceived = false
  let lastUpdateId = snapshot.lastUpdateId

  const tableData = {
    bids: shallowRef(modifyRows(snapshot.bids)),
    asks: shallowRef(modifyRows(snapshot.asks))
  }

  const { send } = useWebSocket('wss://stream.binance.com:9443/ws', {
    onConnected() {
      console.log('Connected')
    },
    onMessage(_, event) {
      const data = JSON.parse(event.data) as Event

      if (data.e !== 'depthUpdate') {
        return
      }

      // only fresh events;
      if (data.u <= snapshot.lastUpdateId) {
        return
      }

      if (!isFirstEventReceived) {
        if (data.U <= lastUpdateId + 1 && data.u >= lastUpdateId + 1) {
          isFirstEventReceived = true

          console.log(`before:`, tableData.asks.value)

          console.log('update:', data.a)

          tableData.asks.value = updateRows(tableData.asks.value, data.a, true)

          console.log(`after`, updateRows(tableData.asks.value, data.a, true))
        }
        lastUpdateId = data.u + 1
        return
      }

      lastUpdateId = data.u + 1
      tableData.asks.value = updateRows(tableData.asks.value, data.a, true)
    }
  })

  send(
    JSON.stringify({
      method: 'SUBSCRIBE',
      params: ['btcusdt@depth'],
      id: 1
    })
  )

  return tableData
}

export const updateRows = (
  currentArr: ModifiedRow[],
  updatesArr: PriceRow[],
  // ascending or descending
  sort: boolean
) => {
  // algorithmic complexity: O(N+K) where K - length of currentArr, N - length of updatesArr

  // for more readability
  const PRICE_INDEX = 0
  const AMOUNT_INDEX = 1

  const updated: ModifiedRow[] = []

  let k = 0 // current arr index

  for (let i = 0; i < updatesArr.length; i++) {
    const updatedAmount = Number(updatesArr[i][AMOUNT_INDEX])
    const updatedPrice = Number(updatesArr[i][PRICE_INDEX])
    const updatedRow: ModifiedRow = {
      price: updatedPrice,
      amount: updatedAmount,
      total: updatedAmount * updatedPrice
    }

    if (updatedAmount === 0 && currentArr.findIndex((el) => el.price === updatedPrice) === -1) {
      continue
    }

    if (sort) {
      // fill updates which must be on top
      if (currentArr[0].price && updatedPrice < currentArr[0].price) {
        updated.push(updatedRow)
        continue
      }

      // fill updates which must be on bottom
      if (k === currentArr.length) {
        updated.push(updatedRow)
        continue
      }

      for (; k < currentArr.length; k++) {
        const currentPrice = currentArr[k].price

        // when equals
        if (updatedPrice === currentPrice) {
          // row doesn't adds when quantity is 0
          updated.push(updatedRow)

          console.log('equals', updatedPrice)
          k++
          break
        }

        const next = currentArr.at(k + 1)
        const prev = updated.at(-1)

        // fill between;
        if (next && prev) {
          console.log('between', prev.price, next.price)

          if (
            prev.price < currentPrice &&
            prev.price < updatedPrice &&
            next.price > currentPrice &&
            next.price > updatedPrice
          ) {
            if (updatedPrice < currentPrice) {
              updated.push(updatedRow)
              updated.push(currentArr[k])
            } else {
              updated.push(currentArr[k])
              updated.push(updatedRow)
            }
            k++
            break
          } else {
            updated.push(currentArr[k])
          }
        }

        // fill when no prev el for current el.
        if (!prev && next && next.price > updatedPrice && next.price > currentPrice) {
          console.log('no prev', next.price)

          if (updatedPrice < currentPrice) {
            updated.push(updatedRow)
            updated.push(currentArr[k])
          } else {
            updated.push(currentArr[k])
            updated.push(updatedRow)
          }
          k++
          break
        }

        // fill when no next el for current el.
        if (!next && prev && prev.price < updatedPrice && prev.price < currentPrice) {
          console.log('no next el', prev.price)
          if (updatedPrice < currentPrice) {
            updated.push(updatedRow)
            updated.push(currentArr[k])
          } else {
            updated.push(currentArr[k])
            updated.push(updatedRow)
          }
          k++
          break
        }

        // fill when length of array is 1
        if (!next && !prev) {
          console.log('no next and no prev el', currentPrice)
          if (updatedPrice < currentPrice) {
            updated.push(updatedRow)
            updated.push(currentArr[k])
          } else {
            updated.push(currentArr[k])
            updated.push(updatedRow)
          }
          k++
          break
        }

        // fill first el when no conditions doesn't match
        if (updated.length === 0) {
          console.log('updated empty', currentPrice)
          updated.push(currentArr[k])
        }
      }
    } else {
      // fill updates which must be on top
      if (currentArr[0].price && updatedPrice > currentArr[0].price) {
        updated.push(updatedRow)
        continue
      }

      // fill updates which must be on bottom
      if (k === currentArr.length) {
        updated.push(updatedRow)
        continue
      }

      for (; k < currentArr.length; k++) {
        const currentPrice = currentArr[k].price

        // when equals
        if (updatedPrice === currentPrice) {
          updated.push(updatedRow)
          k++
          break
        }

        const next = currentArr.at(k + 1)
        const prev = updated.at(-1)

        // fill between;
        if (next && prev) {
          const nextPrice = next.price
          const prevPrice = prev.price

          if (
            prevPrice > currentPrice &&
            prevPrice > updatedPrice &&
            nextPrice < currentPrice &&
            nextPrice < updatedPrice
          ) {
            if (updatedPrice > currentPrice) {
              updated.push(updatedRow)
              updated.push(currentArr[k])
            } else {
              updated.push(currentArr[k])
              updated.push(updatedRow)
            }
            k++
            break
          } else {
            updated.push(currentArr[k])
          }
        }

        // fill when no prev el for current el.
        if (!prev && next && next.price < updatedPrice && next.price < currentPrice) {
          if (updatedPrice > currentPrice) {
            updated.push(updatedRow)
            updated.push(currentArr[k])
          } else {
            updated.push(currentArr[k])
            updated.push(updatedRow)
          }
          k++
          break
        }

        // fill when no next el for current el.
        if (!next && prev && prev.price > updatedPrice && prev.price > currentPrice) {
          if (updatedPrice > currentPrice) {
            updated.push(updatedRow)
            updated.push(currentArr[k])
          } else {
            updated.push(currentArr[k])
            updated.push(updatedRow)
          }
          k++
          break
        }

        // fill when length of array is 1
        if (!next && !prev) {
          if (updatedPrice > currentPrice) {
            updated.push(updatedRow)
            updated.push(currentArr[k])
          } else {
            updated.push(currentArr[k])
            updated.push(updatedRow)
          }
          k++
          break
        }

        // fill first el when no conditions doesn't match
        if (updated.length === 0) {
          updated.push(currentArr[k])
        }
      }
    }
  }

  // fill the rest of current array.
  for (; k < currentArr.length; k++) {
    updated.push(currentArr[k])
  }

  return updated.filter((el) => el.amount !== 0).slice(0, 100)
}
