import { expect, test, describe } from 'vitest'
import type { ModifiedRow } from '@pages/modifyRows'
import type { PriceRow } from '@shared/api'
import { updateRows } from './useBinanceStream'

describe('update rows test suit', () => {
  test('updateRows: fill between', () => {
    const currentArr: ModifiedRow[] = [
      {
        price: 3,
        amount: 1,
        total: 3
      },
      {
        price: 4,
        amount: 1,
        total: 4
      },
      {
        price: 11,
        amount: 1,
        total: 11
      },
      {
        price: 16,
        amount: 1,
        total: 16
      }
    ]

    const update: PriceRow[] = [
      ['2', '1'],
      ['6', '1'],
      ['10', '1'],
      ['13', '1']
    ]

    const updated = updateRows(currentArr, update, true)

    expect(updated).toStrictEqual([
      {
        price: 2,
        amount: 1,
        total: 2
      },
      {
        price: 3,
        amount: 1,
        total: 3
      },
      {
        price: 4,
        amount: 1,
        total: 4
      },
      {
        price: 6,
        amount: 1,
        total: 6
      },
      {
        price: 10,
        amount: 1,
        total: 10
      },
      {
        price: 11,
        amount: 1,
        total: 11
      },
      {
        price: 13,
        amount: 1,
        total: 13
      },
      {
        price: 16,
        amount: 1,
        total: 16
      }
    ])
  })

  test('updateRows: fill between descending', () => {
    const currentArr = [
      {
        price: 3,
        amount: 1,
        total: 3
      },
      {
        price: 4,
        amount: 1,
        total: 4
      },
      {
        price: 11,
        amount: 1,
        total: 11
      },
      {
        price: 16,
        amount: 1,
        total: 16
      }
    ].reverse() as ModifiedRow[]

    const update = [
      ['2', '1'],
      ['6', '1'],
      ['10', '1'],
      ['13', '1']
    ].reverse() as PriceRow[]

    const updated = updateRows(currentArr, update, false)

    expect(updated).toStrictEqual(
      [
        {
          price: 2,
          amount: 1,
          total: 2
        },
        {
          price: 3,
          amount: 1,
          total: 3
        },
        {
          price: 4,
          amount: 1,
          total: 4
        },
        {
          price: 6,
          amount: 1,
          total: 6
        },
        {
          price: 10,
          amount: 1,
          total: 10
        },
        {
          price: 11,
          amount: 1,
          total: 11
        },
        {
          price: 13,
          amount: 1,
          total: 13
        },
        {
          price: 16,
          amount: 1,
          total: 16
        }
      ].reverse()
    )
  })

  test('updateRows: prepend and append', () => {
    const currentArr: ModifiedRow[] = [
      {
        price: 3,
        amount: 1,
        total: 3
      },
      {
        price: 4,
        amount: 1,
        total: 4
      },
      {
        price: 5,
        amount: 1,
        total: 5
      },
      {
        price: 6,
        amount: 1,
        total: 6
      }
    ]

    const update: PriceRow[] = [
      ['1', '1'],
      ['2', '1'],
      ['10', '1'],
      ['13', '1'],
      ['15', '1'],
      ['17', '1']
    ]

    const updated = updateRows(currentArr, update, true)

    expect(updated).toStrictEqual([
      {
        price: 1,
        amount: 1,
        total: 1
      },
      {
        price: 2,
        amount: 1,
        total: 2
      },
      {
        price: 3,
        amount: 1,
        total: 3
      },
      {
        price: 4,
        amount: 1,
        total: 4
      },
      {
        price: 5,
        amount: 1,
        total: 5
      },
      {
        price: 6,
        amount: 1,
        total: 6
      },
      {
        price: 10,
        amount: 1,
        total: 10
      },
      {
        price: 13,
        amount: 1,
        total: 13
      },
      {
        price: 15,
        amount: 1,
        total: 15
      },
      {
        price: 17,
        amount: 1,
        total: 17
      }
    ])
  })

  test('updateRows: prepend and append descending', () => {
    const currentArr: ModifiedRow[] = [
      {
        price: 3,
        amount: 1,
        total: 3
      },
      {
        price: 4,
        amount: 1,
        total: 4
      },
      {
        price: 5,
        amount: 1,
        total: 5
      },
      {
        price: 6,
        amount: 1,
        total: 6
      }
    ].reverse() as ModifiedRow[]

    const update: PriceRow[] = [
      ['1', '1'],
      ['2', '1'],
      ['10', '1'],
      ['13', '1'],
      ['15', '1'],
      ['17', '1']
    ].reverse() as PriceRow[]

    const updated = updateRows(currentArr, update, false)

    expect(updated).toStrictEqual(
      [
        {
          price: 1,
          amount: 1,
          total: 1
        },
        {
          price: 2,
          amount: 1,
          total: 2
        },
        {
          price: 3,
          amount: 1,
          total: 3
        },
        {
          price: 4,
          amount: 1,
          total: 4
        },
        {
          price: 5,
          amount: 1,
          total: 5
        },
        {
          price: 6,
          amount: 1,
          total: 6
        },
        {
          price: 10,
          amount: 1,
          total: 10
        },
        {
          price: 13,
          amount: 1,
          total: 13
        },
        {
          price: 15,
          amount: 1,
          total: 15
        },
        {
          price: 17,
          amount: 1,
          total: 17
        }
      ].reverse()
    )
  })

  test('updateRows: add one element in between', () => {
    const currentArr: ModifiedRow[] = [
      {
        price: 3,
        amount: 1,
        total: 3
      },
      {
        price: 4,
        amount: 1,
        total: 4
      },
      {
        price: 11,
        amount: 1,
        total: 11
      },
      {
        price: 16,
        amount: 1,
        total: 16
      }
    ]

    const update: PriceRow[] = [['10', '1']]

    const updated = updateRows(currentArr, update, true)

    expect(updated).toStrictEqual([
      {
        price: 3,
        amount: 1,
        total: 3
      },
      {
        price: 4,
        amount: 1,
        total: 4
      },
      {
        price: 10,
        amount: 1,
        total: 10
      },
      {
        price: 11,
        amount: 1,
        total: 11
      },
      {
        price: 16,
        amount: 1,
        total: 16
      }
    ])
  })

  test('updateRows: add one element in between descending', () => {
    const currentArr: ModifiedRow[] = [
      {
        price: 3,
        amount: 1,
        total: 3
      },
      {
        price: 4,
        amount: 1,
        total: 4
      },
      {
        price: 11,
        amount: 1,
        total: 11
      },
      {
        price: 16,
        amount: 1,
        total: 16
      }
    ].reverse() as ModifiedRow[]

    const update: PriceRow[] = [['10', '1']].reverse() as PriceRow[]

    const updated = updateRows(currentArr, update, false)

    expect(updated).toStrictEqual(
      [
        {
          price: 3,
          amount: 1,
          total: 3
        },
        {
          price: 4,
          amount: 1,
          total: 4
        },
        {
          price: 10,
          amount: 1,
          total: 10
        },
        {
          price: 11,
          amount: 1,
          total: 11
        },
        {
          price: 16,
          amount: 1,
          total: 16
        }
      ].reverse()
    )
  })

  test('updateRows: delete one element and add one element in between', () => {
    const currentArr: ModifiedRow[] = [
      {
        price: 3,
        amount: 1,
        total: 3
      },
      {
        price: 4,
        amount: 1,
        total: 4
      },
      {
        price: 11,
        amount: 1,
        total: 11
      },
      {
        price: 16,
        amount: 1,
        total: 16
      }
    ]

    const update: PriceRow[] = [
      ['4', '0'],
      ['5', '1']
    ]

    const updated = updateRows(currentArr, update, true)

    expect(updated).toStrictEqual([
      {
        price: 3,
        amount: 1,
        total: 3
      },
      {
        price: 5,
        amount: 1,
        total: 5
      },
      {
        price: 11,
        amount: 1,
        total: 11
      },
      {
        price: 16,
        amount: 1,
        total: 16
      }
    ])
  })

  test('updateRows: delete one element and add one element in between descending', () => {
    const currentArr: ModifiedRow[] = [
      {
        price: 3,
        amount: 1,
        total: 3
      },
      {
        price: 4,
        amount: 1,
        total: 4
      },
      {
        price: 11,
        amount: 1,
        total: 11
      },
      {
        price: 16,
        amount: 1,
        total: 16
      }
    ].reverse() as ModifiedRow[]

    const update: PriceRow[] = [
      ['4', '0'],
      ['5', '1']
    ].reverse() as PriceRow[]

    const updated = updateRows(currentArr, update, false)

    expect(updated).toStrictEqual(
      [
        {
          price: 3,
          amount: 1,
          total: 3
        },
        {
          price: 5,
          amount: 1,
          total: 5
        },
        {
          price: 11,
          amount: 1,
          total: 11
        },
        {
          price: 16,
          amount: 1,
          total: 16
        }
      ].reverse()
    )
  })

  test('updateRows: add one element at start', () => {
    const currentArr: ModifiedRow[] = [
      {
        price: 3,
        amount: 1,
        total: 3
      }
    ]

    const update: PriceRow[] = [['1', '1']]

    const updated = updateRows(currentArr, update, true)

    expect(updated).toStrictEqual([
      /*[1, 1, 1],
      [3, 1, 3]*/
      {
        price: 1,
        amount: 1,
        total: 1
      },
      {
        price: 3,
        amount: 1,
        total: 3
      }
    ])
  })

  test('updateRows: add one element at end', () => {
    const currentArr: ModifiedRow[] = [
      {
        price: 1,
        amount: 1,
        total: 1
      }
    ]

    const update: PriceRow[] = [['3', '1']]

    const updated = updateRows(currentArr, update, true)

    expect(updated).toStrictEqual([
      {
        price: 1,
        amount: 1,
        total: 1
      },
      {
        price: 3,
        amount: 1,
        total: 3
      }
    ])
  })

  test('updateRows: add one element at end descending', () => {
    const currentArr: ModifiedRow[] = [
      {
        price: 3,
        amount: 1,
        total: 3
      }
    ]

    const update: PriceRow[] = [['1', '1']]

    const updated = updateRows(currentArr, update, false)

    expect(updated).toStrictEqual([
      {
        price: 3,
        amount: 1,
        total: 3
      },
      {
        price: 1,
        amount: 1,
        total: 1
      }
    ])
  })

  test('updateRows: all operations', () => {
    const currentArr: ModifiedRow[] = [
      {
        price: 3,
        amount: 1,
        total: 3
      },
      {
        price: 10,
        amount: 1,
        total: 10
      },
      {
        price: 16,
        amount: 1,
        total: 16
      },
      {
        price: 34,
        amount: 1,
        total: 34
      },
      {
        price: 50,
        amount: 1,
        total: 50
      },
      {
        price: 110,
        amount: 1,
        total: 110
      },
      {
        price: 111,
        amount: 1,
        total: 111
      }
    ]

    const update: PriceRow[] = [
      ['1', '1'],
      ['5', '1'],
      ['34', '2'],
      ['50', '0'],
      ['150', '2']
    ]

    const updated = updateRows(currentArr, update, true)

    expect(updated).toStrictEqual([
      {
        price: 1,
        amount: 1,
        total: 1
      },
      {
        price: 3,
        amount: 1,
        total: 3
      },
      {
        price: 5,
        amount: 1,
        total: 5
      },
      {
        price: 10,
        amount: 1,
        total: 10
      },
      {
        price: 16,
        amount: 1,
        total: 16
      },
      {
        price: 34,
        amount: 2,
        total: 68
      },
      {
        price: 110,
        amount: 1,
        total: 110
      },
      {
        price: 111,
        amount: 1,
        total: 111
      },
      {
        price: 150,
        amount: 2,
        total: 300
      }
    ])
  })

  test('updateRows: all operations descending', () => {
    const currentArr: ModifiedRow[] = [
      {
        price: 3,
        amount: 1,
        total: 3
      },
      {
        price: 10,
        amount: 1,
        total: 10
      },
      {
        price: 16,
        amount: 1,
        total: 16
      },
      {
        price: 34,
        amount: 1,
        total: 34
      },
      {
        price: 50,
        amount: 1,
        total: 50
      },
      {
        price: 110,
        amount: 1,
        total: 110
      },
      {
        price: 111,
        amount: 1,
        total: 111
      }
    ].reverse() as ModifiedRow[]

    const update: PriceRow[] = [
      ['1', '1'],
      ['5', '1'],
      ['34', '2'],
      ['50', '0'],
      ['150', '2']
    ].reverse() as PriceRow[]

    const updated = updateRows(currentArr, update, false)

    expect(updated).toStrictEqual(
      [
        {
          price: 1,
          amount: 1,
          total: 1
        },
        {
          price: 3,
          amount: 1,
          total: 3
        },
        {
          price: 5,
          amount: 1,
          total: 5
        },
        {
          price: 10,
          amount: 1,
          total: 10
        },
        {
          price: 16,
          amount: 1,
          total: 16
        },
        {
          price: 34,
          amount: 2,
          total: 68
        },
        {
          price: 110,
          amount: 1,
          total: 110
        },
        {
          price: 111,
          amount: 1,
          total: 111
        },
        {
          price: 150,
          amount: 2,
          total: 300
        }
      ].reverse()
    )
  })

  test('updateRows: remove one', () => {
    const currentArr: ModifiedRow[] = [
      {
        price: 1,
        amount: 1,
        total: 1
      }
    ]

    const update: PriceRow[] = [['1', '0']]

    const updated = updateRows(currentArr, update, true)

    expect(updated).toStrictEqual([])
  })

  test('updateRows: when needs to delete somethings that does not exist', () => {
    const currentArr: ModifiedRow[] = [
      {
        price: 1,
        amount: 1,
        total: 1
      },
      {
        price: 3,
        amount: 1,
        total: 3
      }
    ]

    const update: PriceRow[] = [['2', '0']]

    const updated = updateRows(currentArr, update, true)

    expect(updated).toStrictEqual([
      {
        price: 1,
        amount: 1,
        total: 1
      },
      {
        price: 3,
        amount: 1,
        total: 3
      }
    ])
  })

  test('update rows: delete many items', () => {
    const currentArr: ModifiedRow[] = [
      {
        price: 1,
        amount: 1,
        total: 1
      },
      {
        price: 5,
        amount: 1,
        total: 5
      },
      {
        price: 10,
        amount: 1,
        total: 10
      },
      {
        price: 16,
        amount: 1,
        total: 16
      },
      {
        price: 34,
        amount: 2,
        total: 68
      },
      {
        price: 110,
        amount: 1,
        total: 110
      },
      {
        price: 111,
        amount: 1,
        total: 111
      },
      {
        price: 150,
        amount: 2,
        total: 300
      }
    ]

    const update: PriceRow[] = [
      ['1', '0'],
      ['2', '0'],
      ['3', '0'],
      ['5', '0'],
      ['10', '0'],
      ['16', '0'],
      ['34', '0'],
      ['110', '0'],
      ['111', '0'],
      ['150', '0'],
      ['200', '2']
    ]

    expect(updateRows(currentArr, update, true)).toStrictEqual([
      {
        price: 200,
        amount: 2,
        total: 400
      }
    ])
  })
})
