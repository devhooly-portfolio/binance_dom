import { inject, type Ref } from 'vue'
import { BREAKPOINT_KEY, BREAKPOINTS } from '../const'

export const useBreakpoints = () => {
  // generic will be possibly undefined type
  return inject(BREAKPOINT_KEY) as Record<keyof typeof BREAKPOINTS, Ref<boolean>>
}
