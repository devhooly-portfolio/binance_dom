// [price, quantity]
export type PriceRow = [string, string]

export type Snapshot = {
  lastUpdateId: number
  bids: PriceRow[]
  asks: PriceRow[]
}

export const getSnapshot = async (pair: string, limit: number): Promise<Snapshot> => {
  const response = await fetch(`https://api.binance.com/api/v3/depth?symbol=${pair}&limit=${limit}`)
  if (response.status !== 200) {
    throw new Error(response.statusText)
  }
  return response.json()
}
