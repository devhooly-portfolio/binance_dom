import { useMediaQuery } from '@vueuse/core'
import type { Ref } from 'vue'
import { BREAKPOINTS } from '@shared/const'

type Breakpoints = keyof typeof BREAKPOINTS

export const breakpointsProvider = () => {
  return Object.entries(BREAKPOINTS).reduce<Record<Breakpoints, Ref<boolean>>>(
    (acc, [breakpoint, value]) => {
      acc[breakpoint as Breakpoints] = useMediaQuery(`( min-width: ${value}px )`)
      return acc
    },
    {} as Record<Breakpoints, Ref<boolean>>
  )
}
