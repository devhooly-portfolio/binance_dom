export const BREAKPOINTS = {
  sm: 600,
  md: 960,
  lg: 1280,
  xl: 1920
} as const

export const BREAKPOINT_KEY = Symbol('breakpoints')
