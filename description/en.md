# APPLICATION.
Develop an adaptive application for desktop and mobile platforms, consisting of:
1) Header. In the header add a random logo and menu for two pages (Settings / Order Book).
2) Areas for rendering pages. Pages should be loaded dynamically by separate chunks as needed.
3) Business logic, work with api and data, rendered in the store and broken down into modules.
4) It is desirable to use vuetify as a ui bibliotka, class naming if necessary to implement by BEM.

## DATA
We use api Binance as a source of data necessary for page implementation. Namely 2 methods: get the exchange stack for a certain currency pair via REST and subscribe for stack updates via WS. (See Diff. Depth Stream in the documentation: https://github.com/binance/binance-spot-api-docs/blob/master/web-socket-streams.md). Pay attention to
https://github.com/binance/binance-spot-api-docs/blob/master/web-socket-streams.md#how-to-manage-a-local-order-book-correctly.
Prices should be consistently in the glass + consistency should be observed

## PAGES
1) Page with currency pair settings and log of changes.
   Realize select with currency pair selection(Charcode a list of BTCUSDT, BNBBTC, ETHBTC. BTCUSDT is selected by default). When selected, we update the data in the store (rest method) and connect via ws to update the data on the currency pair.
   Block with the log of our actions to change the currency pair. From which to which we changed and at what time.
   *Example of Order Book and its correct work on https://www.binance.com/ru/trade/BTC_USDT.
2) Page with table (Order Book).
   Display two tables side by side for two arrays (Bids and Asks) with columns: Price, Quantity, Total(Price * Quantity). In mobile version display only two columns(Price, Total).
   Select with selection of the number of items in the table(100, 500, 1000).
   Requirements for tables: on desktop and on mobile together two tables should not exceed the height of the device screen (Scrolling should be inside the tables. When scrolling, the header with column breakdown must remain in place).
   In terms of style (colors, rounding, indents) all at your own discretion, but what would look neat.