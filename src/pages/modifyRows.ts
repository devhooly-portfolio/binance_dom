import type { PriceRow } from '@shared/api'

export type ModifiedRow = {
  price: number
  amount: number
  total: number
}

export const modifyRows = (rows: PriceRow[]): ModifiedRow[] =>
  rows.map((row) => ({ price: +row[0], amount: +row[1], total: +row[0] * +row[1] }))
